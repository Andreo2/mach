#version 330 core
    in vec2 TexCoords;
    out vec4 color;
    uniform sampler2D texture1;
    void main()
    {
        vec3 result=texture(texture1, TexCoords).rgb;
        color=vec4(result,1.0f);
    };

