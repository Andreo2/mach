#version 330 core
    in vec2 TexCoord;

    out vec4 color;
    uniform vec3 objectColor;
    uniform vec3 lightColor;
    uniform sampler2D ourTexture1;
    uniform sampler2D ourTexture2;
    uniform float mixure;
    void main()
    {
        color=mix(texture(ourTexture1, TexCoord), texture(ourTexture2, TexCoord), mixure);
    };

