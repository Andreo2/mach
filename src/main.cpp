#define GLEW_STATIC
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<unistd.h>
#include<iostream>
#include<cmath>
#include<string>
#include"shader.h"
#include"SOIL.h"
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include"vertices"
#include"camera.h"

using namespace std;
void initLib();
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);
void scrollCallback(GLFWwindow *window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow *window, double xpos, double ypos);
void doMovement();
void setDirLight(Shader& shader, glm::vec3 direction, glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular);
void setPointLight(Shader& shader, glm::vec3 *position, glm::vec3 *pointLightColor, 
        float constant, float linear, float quadratic);
GLuint WIDTH=1024;
GLuint HEIGHT=768;
GLfloat mixure;

Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX=WIDTH/2.0, lastY=HEIGHT/2.0;
bool firstMouse=true;

GLfloat deltaTime=0.0f;
GLfloat lastFrame=0.0f;

glm::vec4 lightDirection(-0.2f, -1.0f, -0.3f, 0.0f);
glm::vec4 lightPos(1.2f, 1.0f, 2.0f, 1.0f);

int main(void)
{
    try{
        //glfw init
        initLib();

        //set glfw
        GLFWwindow *window; 
        window=glfwCreateWindow(WIDTH,HEIGHT,"1",NULL,NULL);
        if(!window) throw "can't create window glfw";
        glfwMakeContextCurrent(window);

        int width,height;
        glfwGetFramebufferSize(window,&width,&height);
        glViewport(0,0,width,height);

        //set callback fun for glfw
        glfwSetKeyCallback(window, keyCallback);
        glfwSetCursorPosCallback(window, mouseCallback);
        glfwSetScrollCallback(window, scrollCallback);

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        //init glew
        glewExperimental=GL_TRUE;
        if(glewInit()!=GLEW_OK) throw "can't initialize GLEW";

        glEnable(GL_DEPTH_TEST);

        //get shader
        Shader lightShader("/home/andrew/mach/src/shaders/vert.sh","/home/andrew/mach/src/shaders/frag.sh"); 
        Shader lampShader("/home/andrew/mach/src/shaders/lightVert.sh","/home/andrew/mach/src/shaders/lightFrag.sh"); 

        //bind buffer for GPU
        GLuint VBO, objectVAO, lightVAO;

        glGenVertexArrays(1, &objectVAO);
        glGenBuffers(1, &VBO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);
        glBindVertexArray(objectVAO);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
        glEnableVertexAttribArray(1);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (GLvoid*)(6*sizeof(GLfloat)));
        glEnableVertexAttribArray(2);
        glBindVertexArray(0); 

        glGenVertexArrays(1, &lightVAO);
        glBindVertexArray(lightVAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8*sizeof(GLfloat), (GLvoid*)(6*sizeof(GLfloat)));
        glEnableVertexAttribArray(2);
        glBindVertexArray(0);
        
        //gen texture
        GLuint textureDiffuse, textureSpecular, textureSun;
        glGenTextures(1, &textureDiffuse);
        glBindTexture(GL_TEXTURE_2D, textureDiffuse);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //create textureDiffuse SOIL
        width=0;
        height=0;
        unsigned char* imageDiffuse=SOIL_load_image("pics/wood.jpg", &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageDiffuse);
        glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(imageDiffuse);
        glBindTexture(GL_TEXTURE_2D, 0);

        
        glGenTextures(1, &textureSpecular);
        glBindTexture(GL_TEXTURE_2D, textureSpecular);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //create textureSpecular SOIL
        width=0;
        height=0;
        unsigned char* imageSpecular=SOIL_load_image("pics/conSpecular.png", &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageSpecular);
        glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(imageSpecular);
        glBindTexture(GL_TEXTURE_2D, 0);
        
        glGenTextures(1, &textureSun);
        glBindTexture(GL_TEXTURE_2D, textureSun);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //create textureSpecular SOIL
        width=0;
        height=0;
        unsigned char* imageSun=SOIL_load_image("pics/sun.jpg", &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, imageSun);
        glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(imageSun);
        glBindTexture(GL_TEXTURE_2D, 0);

        glm::vec3 pointLightColor[] = {
            glm::vec3(0.2f, 0.2f, 0.6f),
            glm::vec3(0.3f, 0.3f, 0.7f),
            glm::vec3(0.0f, 0.0f, 0.3f),
            glm::vec3(0.4f, 0.4f, 0.4f)
        };
        while(!glfwWindowShouldClose(window)){
            glfwPollEvents();
            //camera action
            GLfloat currentFrame=glfwGetTime();
            deltaTime=currentFrame-lastFrame;
            lastFrame=currentFrame;
            doMovement();
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
            //setting light
            lightShader.Use();
            lightShader.setInt("material.diffuse", 0);
            lightShader.setInt("material.specular", 1);
            lightShader.setVec3("viewPos", camera.Position);

            //light prop
            setDirLight(lightShader, glm::vec3( -0.2f, -1.0f, -0.3f), glm::vec3( 0.05f, 0.05f, 0.1f), glm::vec3( 0.2f, 0.2f, 0.7f),
                    glm::vec3( 0.7f, 0.7f, 0.7f));
            setPointLight(lightShader, pointLightPositions,  pointLightColor, 1.0f, 0.09f, 0.032f);
            //setting material
            lightShader.setVec3("material.specular", glm::vec3(0.5f, 0.5f, 0.5f));
            lightShader.setFloat("material.shininess", 64.0f);

            //camera transformations
            glm::mat4 view=camera.GetViewMatrix();
            glm::mat4 projection=glm::perspective(camera.Zoom, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 1000.0f);
            lightShader.setMat4("view", view);
            lightShader.setMat4("projection", projection);
            
            glm::mat4 model(1.0f);
            //model=glm::rotate(model, currentFrame, glm::vec3(1.0f, 1.0f, 0.0f));
            //lightShader.setMat4("model", model);

            //bind texture
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, textureDiffuse);
            
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, textureSpecular);

            glBindVertexArray(objectVAO);
            for(unsigned int i = 0; i < 10; i++)
            {
                glm::mat4 model(1.0f);
                model = glm::translate(model, cubePositions[i]);
                float angle = 20.0f * i;
                model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
                lightShader.setMat4("model", model);
                glDrawArrays(GL_TRIANGLES, 0, 36);
            }
            glBindVertexArray(0);
            lampShader.Use();
            lampShader.setInt("texture1",2);
            lampShader.setMat4("view", view);
            lampShader.setMat4("projection", projection);

            model=glm::mat4(1.0f);
            model=glm::translate(model, glm::vec3(20.0f, 100.0f, 30.0f));
            model=glm::scale(model, glm::vec3(5.0f));
            lampShader.setMat4("model", model);

            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, textureSun);

            glBindVertexArray(lightVAO);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            for(unsigned int i=0; i<4; i++){
                model = glm::mat4(1.0f);
                model=glm::translate(model, pointLightPositions[i]);
                model=glm::scale(model, glm::vec3(0.2f));
                lampShader.setMat4("model", model);
                glDrawArrays(GL_TRIANGLES, 0, 36);
            }
            glBindVertexArray(0);

            glfwSwapBuffers(window);
        }
        glDeleteVertexArrays(1,&lightVAO);
        glDeleteVertexArrays(1,&objectVAO);
        glDeleteBuffers(1,&VBO);
    }
    catch(const char *x){
        cout<<x<<endl;
        glfwTerminate();
        return -1;
    }
    catch(string x){
        cout<<x<<endl;
        glfwTerminate();
        return -1;
    }
    glfwTerminate();
    return 0;
}

void doMovement(){
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode){
    if(key==GLFW_KEY_ESCAPE && action==GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if(key>=0 && key<1024){
        if(action==GLFW_PRESS)
            keys[key]=true;
        else
            if(action==GLFW_RELEASE)
            keys[key]=false;
    }
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos){
    if(firstMouse){
        lastX=xpos;
        lastY=ypos;
        firstMouse=false;
    }
    GLfloat xoffset=xpos-lastX;
    GLfloat yoffset=lastY-ypos;
    lastX=xpos;
    lastY=ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset){
    camera.ProcessMouseScroll(yoffset);
}

void initLib(){
    if(!glfwInit()){
        throw "can't init glfw";
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}
void setDirLight(Shader& shader, glm::vec3 direction, glm::vec3 ambient, glm::vec3 diffuse, glm::vec3 specular){
    shader.setVec3("dirLight.direction", direction);
    shader.setVec3("dirLight.ambient", ambient);
    shader.setVec3("dirLight.diffuse", diffuse);
    shader.setVec3("dirLight.specular", specular);
}

void setPointLight(Shader& shader, glm::vec3 *position, glm::vec3 *pointLightColor,
       float constant, float linear, float quadratic){
    shader.setVec3("pointLight[0].position", position[0]);
    shader.setVec3("pointLight[0].ambient", 0.1f*pointLightColor[0]);
    shader.setVec3("pointLight[0].diffuse", pointLightColor[0]);
    shader.setVec3("pointLight[0].specular", pointLightColor[0]);
    shader.setFloat("pointLight[0].constant", constant);
    shader.setFloat("pointLight[0].linear", linear);
    shader.setFloat("pointLight[0].quadratic", quadratic);

    shader.setVec3("pointLight[1].position", position[1]);
    shader.setVec3("pointLight[1].ambient", 0.1f*pointLightColor[1]);
    shader.setVec3("pointLight[1].diffuse", pointLightColor[1]);
    shader.setVec3("pointLight[1].specular", pointLightColor[1]);
    shader.setFloat("pointLight[1].constant", constant);
    shader.setFloat("pointLight[1].linear", linear);
    shader.setFloat("pointLight[1].quadratic", quadratic);

    shader.setVec3("pointLight[2].position", position[2]);
    shader.setVec3("pointLight[2].ambient", 0.1f*pointLightColor[2]);
    shader.setVec3("pointLight[2].diffuse", pointLightColor[2]);
    shader.setVec3("pointLight[2].specular", pointLightColor[2]);
    shader.setFloat("pointLight[2].constant", constant);
    shader.setFloat("pointLight[2].linear", linear);
    shader.setFloat("pointLight[2].quadratic", quadratic);

    shader.setVec3("pointLight[3].position", position[3]);
    shader.setVec3("pointLight[3].ambient", 0.1f*pointLightColor[3]);
    shader.setVec3("pointLight[3].diffuse", pointLightColor[3]);
    shader.setVec3("pointLight[3].specular", pointLightColor[3]);
    shader.setFloat("pointLight[3].constant", constant);
    shader.setFloat("pointLight[3].linear", linear);
    shader.setFloat("pointLight[3].quadratic", quadratic);
}
