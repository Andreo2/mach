#define GLEW_STATIC
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include<unistd.h>
#include<iostream>
#include<cmath>
#include<string>
#include"shader.h"
#include"SOIL.h"
#include<glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>
#include"vertices"
#include"camera.h"

using namespace std;
void initLib();
void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode);
void scrollCallback(GLFWwindow *window, double xoffset, double yoffset);
void mouseCallback(GLFWwindow *window, double xpos, double ypos);
void doMovement();

GLuint WIDTH=1900;
GLuint HEIGHT=1000;
GLfloat mixure;

Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
bool keys[1024];
GLfloat lastX=WIDTH/2.0, lastY=HEIGHT/2.0;
bool firstMouse=true;

GLfloat deltaTime=0.0f;
GLfloat lastFrame=0.0f;

glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

int main(void)
{
    try{
        //glfw init
        initLib();

        //set glfw
        GLFWwindow *window; 
        window=glfwCreateWindow(WIDTH,HEIGHT,"1",NULL,NULL);
        if(!window) throw "can't create window glfw";
        glfwMakeContextCurrent(window);

        int width,height;
        glfwGetFramebufferSize(window,&width,&height);
        glViewport(0,0,width,height);

        //set callback fun for glfw
        glfwSetKeyCallback(window, keyCallback);
        glfwSetCursorPosCallback(window, mouseCallback);
        glfwSetScrollCallback(window, scrollCallback);

        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

        //init glew
        glewExperimental=GL_TRUE;
        if(glewInit()!=GLEW_OK) throw "can't initialize GLEW";

        glEnable(GL_DEPTH_TEST);

        //get shader
        Shader ourShader("/home/andrew/mach/src/shaders/vert.sh","/home/andrew/mach/src/shaders/frag.sh"); 
        Shader lightShader("/home/andrew/mach/src/shaders/lightVert.sh","/home/andrew/mach/src/shaders/lightFrag.sh"); 

        //bind buffer for GPU
        GLuint VBO, VAO, lightVAO;

        glGenVertexArrays(1, &VAO);
        glGenBuffers(1, &VBO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

        glBindVertexArray(VAO);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        //glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
        //glEnableVertexAttribArray(1);

        glBindVertexArray(0); 

        glGenVertexArrays(1, &lightVAO);

        glBindVertexArray(lightVAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5*sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);
        glBindVertexArray(0);

        //gen texture

        GLuint texture1;
        GLuint texture2;
        
        glGenTextures(1, &texture1);
        glBindTexture(GL_TEXTURE_2D, texture1);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //create texture1 SOIL
        
        width=0;
        height=0;
        unsigned char* image=SOIL_load_image("pics/goblin.png", &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(image);
        glBindTexture(GL_TEXTURE_2D, 0);

        //gen texture2
        
        glGenTextures(1, &texture2);
        glBindTexture(GL_TEXTURE_2D, texture2);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        //create texture2 SOIL

        image=SOIL_load_image("pics/mgadan.png", &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
        glGenerateMipmap(GL_TEXTURE_2D);
        SOIL_free_image_data(image);
        glBindTexture(GL_TEXTURE_2D, 0);

        glClearColor(0.1f,0.1f,0.1f,1.0f);
        while(!glfwWindowShouldClose(window)){
            //set frame time
            GLfloat currentFrame=glfwGetTime();
            deltaTime=currentFrame-lastFrame;
            lastFrame=currentFrame;

            //
            glfwPollEvents();
            doMovement();
            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
            ourShader.Use();
/*            //bind texture
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, texture1);
            glUniform1i(glGetUniformLocation(ourShader.Program, "ourTexture1"), 0);

            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, texture2);
            glUniform1i(glGetUniformLocation(ourShader.Program, "ourTexture2"), 1);
*/            
            //create camera
            glm::mat4 view(1.0f);
            view=camera.GetViewMatrix();
            glm::mat4 projection(1.0f);
            projection=glm::perspective(camera.Zoom, (GLfloat)WIDTH/(GLfloat)HEIGHT, 0.1f, 100.f);
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"view"), 1, GL_FALSE, glm::value_ptr(view));           
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"projection"), 1, GL_FALSE, glm::value_ptr(projection));

            //draw

            glUniform3f(glGetUniformLocation(ourShader.Program, "objectColor"), 1.0f, 0.5f, 0.31f);
            glUniform3f(glGetUniformLocation(ourShader.Program, "lightColor"), 1.0f, 0.5f, 1.0f);
            glBindVertexArray(VAO);
            glm::mat4 model(1.0f);
            model = glm::rotate(model, glm::radians((GLfloat)glfwGetTime() * 50.0f), glm::vec3(0.5f, 1.0f, 0.0f));
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"model"), 1, GL_FALSE, glm::value_ptr(model)); 
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);

            lightShader.Use();
            model=glm::mat4(1.0f);
            model=glm::translate(model, lightPos);
            model=glm::scale(model, glm::vec3(0.2f));
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"model"), 1, GL_FALSE, glm::value_ptr(model));           
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"view"), 1, GL_FALSE, glm::value_ptr(view));           
            glUniformMatrix4fv(glGetUniformLocation(ourShader.Program,"projection"), 1, GL_FALSE, glm::value_ptr(projection));
            glBindVertexArray(lightVAO);
            glDrawArrays(GL_TRIANGLES, 0, 36);
            glBindVertexArray(0);

            glfwSwapBuffers(window);
        }
        glDeleteVertexArrays(1,&lightVAO);
        glDeleteVertexArrays(1,&VAO);
        glDeleteBuffers(1,&VBO);
    }
    catch(const char *x){
        cout<<x<<endl;
        glfwTerminate();
        return -1;
    }
    catch(string x){
        cout<<x<<endl;
        glfwTerminate();
        return -1;
    }
    glfwTerminate();
    return 0;
}

void doMovement(){
    if(keys[GLFW_KEY_W])
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if(keys[GLFW_KEY_S])
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if(keys[GLFW_KEY_D])
        camera.ProcessKeyboard(RIGHT, deltaTime);
    if(keys[GLFW_KEY_A])
        camera.ProcessKeyboard(LEFT, deltaTime);
}

void keyCallback(GLFWwindow *window, int key, int scancode, int action, int mode){
    if(key==GLFW_KEY_ESCAPE && action==GLFW_PRESS)
        glfwSetWindowShouldClose(window, GL_TRUE);
    if(key>=0 && key<1024){
        if(action==GLFW_PRESS)
            keys[key]=true;
        else
            if(action==GLFW_RELEASE)
            keys[key]=false;
    }
}

void mouseCallback(GLFWwindow* window, double xpos, double ypos){
    if(firstMouse){
        lastX=xpos;
        lastY=ypos;
        firstMouse=false;
    }
    GLfloat xoffset=xpos-lastX;
    GLfloat yoffset=lastY-ypos;
    lastX=xpos;
    lastY=ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset){
    camera.ProcessMouseScroll(yoffset);
}

void initLib(){
    if(!glfwInit()){
        throw "can't init glfw";
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
}
